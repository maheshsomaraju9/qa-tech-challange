Feature: Searching For Hotels

In order to book the hotel accommodation for specific dates and locations
I want to be able to search for hotels by specific keywords and dates



  Scenario: Search for hotels in a specified city
    Given I am on Travels Home Page
    When I enter city name 'Auckland' for search
    When I choose Check-In Date as '28/09/2018'
    When And I choose Check-Out Date as '05/10/2018'
    When And I enter Hotel Guests as '2'
    When I click Hotel Search