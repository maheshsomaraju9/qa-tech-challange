Feature: Searching For Flights

In order to book the flights for desired dates and between choosen cities
I want to be able to search for flights between specified cities and dates and should get appropriate results



  Scenario: Search for flights between choosen cities and dates
    Given I am on Travels Home Page
	When I click on Flights Tab
	When I enter from city name 'Auckland' for search
	When I enter to city name 'Sydney' for search
	When I choose Flight Depart Date as '04/11/2018'
	# When I choose Flight Return Date as '05/11/2018'
	When And I choose Flight Passengers as '2'
	When I click Flight Search
	Then I should see flight search results
	Then I should see FROM Airport Name as 'Auckland' in Search Results
	Then I should see TO Airport Name as 'Sydney' in Search Results
