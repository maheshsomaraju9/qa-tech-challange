package com.quantas.test.travels.booking.steps;

import com.quantas.test.travels.booking.pages.HomePage;
import net.thucydides.core.annotations.Step;

public class HotelSearchSteps {

    HomePage homePage;

    @Step
    public void openTravelsHomePage() {
        homePage.open();
    }

    @Step
    public void clickOnHotelstab() {
        homePage.clickHotelsTab();
    }


    @Step
    public void enterSearchHotelOrCityName(String strHotelOrCityName) throws InterruptedException {
        homePage.enterHotelOrCityName(strHotelOrCityName);
    }


    @Step
    public void enterCheckInDate(String strCheckInDate) {
        homePage.enterHotelCheckInDate(strCheckInDate);
    }

    @Step
    public void enterCheckOutDate(String strCheckOutDate) {
        homePage.enterHotelCheckOutDate(strCheckOutDate);
    }

    @Step
    public void enterGuestsForHotel(String strHotelGuests) {
        homePage.enterHotelGuests(strHotelGuests);
    }


    @Step
    public void searchForHotels() {
        homePage.clickSearchHotels();
    }




}
