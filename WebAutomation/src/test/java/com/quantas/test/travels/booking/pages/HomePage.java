package com.quantas.test.travels.booking.pages;

import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.*;
import org.openqa.selenium.support.*;
import org.openqa.selenium.support.ui.Select;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;


/**
 * Page Objects for Home Page - PHP Travels

 *
 * @author      Mahesh Somaraju <maheshsomaraj@gmail.com>
 * @version     .1
 * @since       .1
 */

@DefaultUrl("https://www.phptravels.net/")
public class HomePage extends PageObject {

    private static final Logger _log = LoggerFactory.getLogger(HomePage.class);

    //------------------------------            Start - Hotel Fields                --------------------------
    @FindBy(xpath = "//a[@href='#HOTELS']")
    WebElement btnHotelsTab;

    @FindBy(xpath = "(//span[text()='Search by Hotel or City Name'])[1]")
    WebElement lblSearchByHotel;

    @FindBy(xpath = "//li[text()='Please enter 3 more characters']/../..//input")
    WebElement txtHotelOrCityName;

    @FindBy(xpath = "//div[@id='HOTELS']//input[@placeholder='Check in']")
    WebElement txtHotelCheckIn;

    @FindBy(xpath = "//div[contains(@style,'display: block;') and @class='datepicker dropdown-menu']//td[@class='day  active']")
    WebElement btnCheckInDatePicker;

    @FindBy(xpath = "//div[@id='HOTELS']//input[@placeholder='Check out']")
    WebElement txtHotelCheckOut;

    @FindBy(xpath = "//div[contains(@style,'display: block;') and @class='datepicker dropdown-menu']//td[@class='day  active']")
    WebElement btnCheckOutDatePicker;

    @FindBy(xpath = "//div[@id='HOTELS']//input[@name='travellers']")
    WebElement txtHotelTravellers;

    @FindBy(xpath = "//div[@id='HOTELS']//button[@type='submit']")
    WebElement btnHotelSearch;
    //------------------------------            End - Hotel Fields                --------------------------


    //------------------------------            Start - Flight Fields                --------------------------
    @FindBy(xpath = "//a[@href='#flights']")
    WebElement btnFlightsTab;

    @FindBy(xpath = "(//span[text()='Enter City Or Airport'])[1]")
    WebElement lblFromAirportOrCityName;

    @FindBy(xpath = "//li[text()='Please enter 3 more characters']/../..//input")
    WebElement txtFromAirportOrCityName;

    @FindBy(xpath = "(//span[text()='Enter City Or Airport'])[1]")  //Assuming From Airport Lable already cleared
    WebElement lblToAirportOrCityName;

    @FindBy(xpath = "//li[text()='Please enter 3 more characters']/../..//input")
    WebElement txtToAirportOrCityName;

    @FindBy(xpath = "//input[@placeholder='Depart']")
    WebElement txtDepartDate;

    @FindBy(xpath = "//div[contains(@style,'display: block;') and @class='datepicker dropdown-menu']//td[@class='day  active']")
    WebElement btnDepartDatePicker;

    @FindBy(xpath = "//input[@placeholder='Return']")
    WebElement txtReturnDate;

    @FindBy(xpath = "//div[contains(@style,'display: block;') and @class='datepicker dropdown-menu']//td[@class='day  active']")
    WebElement btnReturnDatePicker;

    @FindBy(xpath = "//input[@name='totalManualPassenger']")
    WebElement txtFlightPassengers;

    @FindBy(xpath = "//select[@name='madult']")
    WebElement listAdultsPassengers;

    //Hardcoded to Passengers
    @FindBy(xpath = "//select[@name='madult']/option[@value='2']")
    WebElement optionAdultPassengers;

    @FindBy(xpath = "//button[@id='sumManualPassenger']")
    WebElement btnGuestsDone;

    @FindBy(xpath = "//div[@id='flights']//button[@type='submit']")
    WebElement btnSearchFlights;
    //------------------------------            End - Flight Fields                --------------------------



    //------------------------------            Start - Tour Fields                --------------------------
    @FindBy(xpath = "//a[@href='#TOURS']")
    WebElement btnToursTab;

    @FindBy(xpath = "(//span[text()='Search by Listing or City Name'])[1]")
    WebElement txtToursCityName;

    @FindBy(xpath = "//div[@id='TOURS']//input[@placeholder='Check in']")
    WebElement txtToursCheckIn;

    @FindBy(xpath = "//div[@id='TOURS']//select[@id='adults']")
    WebElement listTourGuests;

    @FindBy(xpath = "//div[@id='TOURS']//select[@id='adults']/option[@value='2']")
    WebElement optionTourGuests;

    @FindBy(xpath = "//div[@id='TOURS']//button[@type='submit']")
    WebElement btnSearchTours;

    //------------------------------            End - Tour Fields                --------------------------



    @FindBy(xpath = "//a[@href='#CARS']")
    WebElement btnCarsTab;

    @FindBy(xpath = "//a[@href='#VISA']")
    WebElement btnVisaTab;





    //------------------------------------              BEGIN - Home Page Actions           ------------------------------------------

    public void HomePage(){
        getDriver().manage().window().maximize();
    }

    //------------------------------            Start - Hotel Search Actions                --------------------------

    public void clickHotelsTab() {
        //Load Selenium Elements
        PageFactory.initElements(getDriver(), this);
        btnHotelsTab.click();
    }

    public void enterHotelOrCityName(String hotelSearchKey) throws InterruptedException {

        //Reload Page elements in case Selenium failed to capture in first place
        PageFactory.initElements(getDriver(), this);

        //Clear the Span field to get text field
        lblSearchByHotel.click();
        //Enter search text
        txtHotelOrCityName.sendKeys(hotelSearchKey);

        //Dynamically capture the first result from Search
        getDriver().findElement(By.xpath("(//span[text()='"+ hotelSearchKey +"'])[1]")).click();


    }

    public void enterHotelCheckInDate(String hotelCheckInDate) {

        //Reload Page elements in case Selenium failed to capture in first place
        PageFactory.initElements(getDriver(), this);

        //Enter Check-in Date
        //Date Format - 24/09/2018
        txtHotelCheckIn.clear();
        txtHotelCheckIn.sendKeys(hotelCheckInDate);

        //Click on Selected Date in Date-picker pop-up
        btnCheckInDatePicker.click();

    }

    public void enterHotelCheckOutDate(String hotelCheckOutDate) {

        //Reload Page elements in case Selenium failed to capture in first place
        PageFactory.initElements(getDriver(), this);

        //Enter Check-out Date
        //Date Format - 24/09/2018
        txtHotelCheckOut.clear();
        txtHotelCheckOut.sendKeys(hotelCheckOutDate);

        //Click on Selected Date in Date-picker pop-up
        btnCheckOutDatePicker.click();

    }

    public void enterHotelGuests(String hotelGuestsCount) {
        //Reload Page elements in case Selenium failed to capture in first place
        PageFactory.initElements(getDriver(), this);

        //Enter no.of guests
        txtHotelTravellers.clear();
        txtHotelTravellers.sendKeys(hotelGuestsCount);
    }

    public void clickSearchHotels() {

        //Reload Page elements in case Selenium failed to capture in first place
        PageFactory.initElements(getDriver(), this);

        //Click on Search Button
        btnHotelSearch.click();


    }

    //------------------------------            End - Hotel Search Actions                --------------------------




    //------------------------------            Start - Flight Search Actions                --------------------------

    public void clickFlightsTab() {
        //Load Selenium Elements
        PageFactory.initElements(getDriver(), this);
        btnFlightsTab.click();
    }


    public void enterFromCityName(String fromCity) throws InterruptedException {

        //Reload Page elements in case Selenium failed to capture in first place
        PageFactory.initElements(getDriver(), this);

        //Clear the Span field to get text field
        lblFromAirportOrCityName.click();
        //Enter search text
        txtFromAirportOrCityName.sendKeys(fromCity);

        //Dynamically capture the first result from Search
        getDriver().findElement(By.xpath("(//span[text()='"+ fromCity +"'])[1]")).click();


    }


    public void enterToCityName(String toCity) throws InterruptedException {

        //Reload Page elements in case Selenium failed to capture in first place
        PageFactory.initElements(getDriver(), this);

        //Clear the Span field to get text field
        lblToAirportOrCityName.click();
        //Enter search text
        txtToAirportOrCityName.sendKeys(toCity);

        //Dynamically capture the first result from Search
        getDriver().findElement(By.xpath("(//span[text()='"+ toCity +"'])[1]")).click();


    }


    public void enterFlightDepartDate(String flightDepartDate) {

        //Reload Page elements in case Selenium failed to capture in first place
        PageFactory.initElements(getDriver(), this);

        //Enter Check-out Date
        //Date Format - 24/09/2018
        txtDepartDate.clear();
        txtDepartDate.sendKeys(flightDepartDate);

        //Click on Selected Date in Date-picker pop-up
        btnDepartDatePicker.click();

    }



    public void enterFlightReturnDate(String flightReturnDate) {

        //Reload Page elements in case Selenium failed to capture in first place
        PageFactory.initElements(getDriver(), this);

        //Enter Check-out Date
        //Date Format - 24/09/2018
        txtReturnDate.clear();
        txtReturnDate.sendKeys(flightReturnDate);

        //Click on Selected Date in Date-picker pop-up
        btnReturnDatePicker.click();

    }

    public void enterFlightPassengers(String flightPassengersCount) {

        //Reload Page elements in case Selenium failed to capture in first place
        PageFactory.initElements(getDriver(), this);

        //Select No.of Passengers from DropDown
        //new Select(getDriver().findElement(By.xpath("//select[@name='madult']"))).selectByVisibleText(flightPassengersCount);

        //Click on  Passengers Input
        txtFlightPassengers.click();

        WebDriverWait wait = new WebDriverWait(getDriver(), 10);
        //WebElement ddFlightAdultPassengers = getDriver().findElement(By.xpath("//select[@name='madult']"));
        WebElement ddFlightAdultPassengersOptions = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//select[@name='madult']/option)[1]")));
        WebElement ddFlightAdultPassengers = getDriver().findElement(By.xpath("//select[@name='madult']"));
        new Select(ddFlightAdultPassengers).selectByVisibleText(flightPassengersCount);



        //Click on Done
        btnGuestsDone.click();

    }


    public void clickSearchFlights() {

        //Reload Page elements in case Selenium failed to capture in first place
        PageFactory.initElements(getDriver(), this);

        //Click on Search Button
        btnSearchFlights.click();


    }















    //------------------------------            End - Flight Search Actions                --------------------------



}
