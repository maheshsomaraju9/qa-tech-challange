package com.quantas.test.travels.booking.stepdefs;

import com.quantas.test.travels.booking.steps.HotelSearchSteps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

/**
 * Step Definitions for Hotel Search Results - PHP Travels

 *
 * @author      Mahesh Somaraju <maheshsomaraj@gmail.com>
 * @version     .1
 * @since       .1
 */

public class HotelSearchStepDefinitions {

    @Steps
    HotelSearchSteps hotelSearch;

    @Given("I am on Travels Home Page")
    public void launchTravelsHomePage() {
        hotelSearch.openTravelsHomePage();
    }


    @When("I enter city name '(.*)' for search")
    public void enterCityNameForHotelSearch(String strHotelSearchKeyword) throws InterruptedException {
        hotelSearch.enterSearchHotelOrCityName(strHotelSearchKeyword);
    }

    @When("I choose Check-In Date as '(.*)'")
    public void enterCheckInDateForHotelSearch(String strCheckInDate) {
        hotelSearch.enterCheckInDate(strCheckInDate);
    }

    @When("And I choose Check-Out Date as '(.*)'")
    public void enterCheckOutDateForHotelSearch(String strCheckOutDate) {
        hotelSearch.enterCheckOutDate(strCheckOutDate);
    }

    @When("And I enter Hotel Guests as '(.*)'")
    public void enterGuestsForHotel(String strEnterGuestsForHotel) {
        hotelSearch.enterGuestsForHotel(strEnterGuestsForHotel);
    }

    @When("I click Hotel Search")
    public void clickHotelSearchButton() {
        hotelSearch.searchForHotels();
    }

    /******
     *
     * No Hotel Search Results are available for further development
     *
     */
}
