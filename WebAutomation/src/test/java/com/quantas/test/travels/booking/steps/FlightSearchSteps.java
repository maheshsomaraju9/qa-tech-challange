package com.quantas.test.travels.booking.steps;

import com.quantas.test.travels.booking.pages.FlightSearchResultsPage;
import com.quantas.test.travels.booking.pages.HomePage;
import net.thucydides.core.annotations.Step;

public class FlightSearchSteps {

    HomePage homePage;
    FlightSearchResultsPage flightSearchResults;

    @Step
    public void openTravelsHomePage() {
        homePage.open();
    }

    @Step
    public void clickOnFlightstab() {
        homePage.clickFlightsTab();
    }


    @Step
    public void enterFlightFromCityName(String strFlightFromCity) throws InterruptedException {
        homePage.enterFromCityName(strFlightFromCity);
    }

    @Step
    public void enterFlightToCityName(String strFlightToCity) throws InterruptedException {
        homePage.enterToCityName(strFlightToCity);
    }


    @Step
    public void enterDepartDate(String strDepartDate) {
        homePage.enterFlightDepartDate(strDepartDate);
    }

    @Step
    public void enterReturnDate(String strReturnDate) {
        homePage.enterFlightReturnDate(strReturnDate);
    }

    @Step
    public void enterPassengersForFlight(String strFlightPassengers) {
        homePage.enterFlightPassengers(strFlightPassengers);
    }


    @Step
    public void searchForFlights() {
        homePage.clickSearchFlights();
    }


    @Step
    public int getSearchResultsCount() {
        return flightSearchResults.getFlightSearchCount();
    }

    @Step
    public String getFromAirportNameInResults() {
        return flightSearchResults.getFlightsFromAirportName();
    }

    @Step
    public String getToAirportNameInResults() {
        return flightSearchResults.getFlightsToAirportName();
    }


}
