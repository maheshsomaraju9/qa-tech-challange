package com.quantas.test.travels.booking.stepdefs;

import com.quantas.test.travels.booking.pages.FlightSearchResultsPage;
import com.quantas.test.travels.booking.steps.FlightSearchSteps;
import com.quantas.test.travels.booking.steps.HotelSearchSteps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Step Definitions for Flight Search Results - PHP Travels

 *
 * @author      Mahesh Somaraju <maheshsomaraj@gmail.com>
 * @version     .1
 * @since       .1
 */

public class FlightSearchStepDefinitions {

    private static final Logger _log = LoggerFactory.getLogger(FlightSearchStepDefinitions.class);

    @Steps
    FlightSearchSteps flightSearch;


    @When("I click on Flights Tab")
    public void clickOnFlightsTab() {
        flightSearch.clickOnFlightstab();
    }


    @When("I enter from city name '(.*)' for search")
    public void enterFlightFromCityName(String strFlightFromCity) throws InterruptedException {
        flightSearch.enterFlightFromCityName(strFlightFromCity);
    }

    @When("I enter to city name '(.*)' for search")
    public void enterFlightToCityName(String strFlightToCity) throws InterruptedException {
        flightSearch.enterFlightToCityName(strFlightToCity);
    }

    @When("I choose Flight Depart Date as '(.*)'")
    public void enterFlightDepartDate(String strDepartDate) {
        flightSearch.enterDepartDate(strDepartDate);
    }

    @When("I choose Flight Return Date as '(.*)'")
    public void enterFlightReturnDate(String strReturnDate) {
        flightSearch.enterReturnDate(strReturnDate);
    }

    @When("And I choose Flight Passengers as '(.*)'")
    public void enterFlightPassengers(String strFlightPassengersCount) {
        flightSearch.enterPassengersForFlight(strFlightPassengersCount);
    }

    @When("I click Flight Search")
    public void clickFlightSearchButton() {
        flightSearch.searchForFlights();
    }

    @Then("I should see flight search results")
    public void checkFlightSearchResultsCount() {
        //Check if atleast 1 search result exist
        Assert.assertTrue(flightSearch.getSearchResultsCount() > 0);


    }


    @Then("I should see FROM Airport Name as '(.*)' in Search Results")
    public void checkFlightFromAirportNameInResults(String strFromAirportName) {
        //Check if atleast 1 search result exist
        Assert.assertTrue(flightSearch.getFromAirportNameInResults().contains(strFromAirportName));
        _log.info("Successfully verified the From Airport Name: "+ strFromAirportName +" in Search Results");
    }


    @Then("I should see TO Airport Name as '(.*)' in Search Results")
    public void checkFlightToAirportNameInResults(String strToAirportName) {
        //Check if atleast 1 search result exist
        Assert.assertTrue(flightSearch.getToAirportNameInResults().contains(strToAirportName));
        _log.info("Successfully verified the To Airport Name: "+ strToAirportName +" in Search Results");

        _log.info("No. of flights found : "+ flightSearch.getSearchResultsCount());

    }



}
