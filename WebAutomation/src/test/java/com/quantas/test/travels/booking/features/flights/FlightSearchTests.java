package com.quantas.test.travels.booking.features.flights;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features="src/test/resources/features/flights", glue = {"com.quantas.test.travels.booking.stepdefs"})
public class FlightSearchTests {
}
