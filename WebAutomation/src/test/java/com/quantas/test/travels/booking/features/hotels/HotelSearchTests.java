package com.quantas.test.travels.booking.features.hotels;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import net.thucydides.core.annotations.Managed;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features="src/test/resources/features/hotels", glue = {"com.quantas.test.travels.booking.stepdefs"})
public class HotelSearchTests {


}
