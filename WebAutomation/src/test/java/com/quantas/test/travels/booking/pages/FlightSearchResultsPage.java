package com.quantas.test.travels.booking.pages;

import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Page Objects for Flight Search Results Page - PHP Travels

 *
 * @author      Mahesh Somaraju <maheshsomaraj@gmail.com>
 * @version     .1
 * @since       .1
 */

public class FlightSearchResultsPage extends PageObject {
    private static final Logger _log = LoggerFactory.getLogger(FlightSearchResultsPage.class);


    @FindBy(xpath = "(//tr/td[contains(@class,'wow fadeIn')]//span)[1]")
    WebElement lblFromAirportName;

    @FindBy(xpath = "(//tr/td[contains(@class,'wow fadeIn')]//span)[2]")
    WebElement lblToAirportName;



    public String getFlightsFromAirportName() {
        //Load Selenium Elements
        //Xpath for Element - (//tr/td[contains(@class,'wow fadeIn')]//span)[1][contains(@data-original-title,'Auckland')]
        PageFactory.initElements(getDriver(), this);
        return lblFromAirportName.getAttribute("data-original-title");
    }


    public String getFlightsToAirportName() {
        //Load Selenium Elements
        //Xpath for Element - (//tr/td[contains(@class,'wow fadeIn')]//span)[1][contains(@data-original-title,'Auckland')]
        PageFactory.initElements(getDriver(), this);
        return lblToAirportName.getAttribute("data-original-title");
    }


    public int getFlightSearchCount() {

        boolean present;
        try {
            getDriver().findElement(By.xpath("//table[contains(@class,'flights')]//tr"));
            present = true;
        } catch (NoSuchElementException e) {
            present = false;
        }

        if(present){
            return getDriver().findElements(By.xpath("//table[contains(@class,'flights')]//tr")).size();
        }else{
            return 0;
        }

    }



}
