# WEB Automation

## Instruction for you mission:

Go to URL : http://phptravels.com/demo/

Create an automated smoke test suite using Java, Serenity, Cucumber (Preferred) or your framework of choice for the below ACs (you can do some manual set up, but this must be documented)

AC1. I want to be able book hotels, flights or tours
Provide a reports of the execution and any bugs found ( be creative )



# Java #
The project SDK version is 1.8
The project language level is 8 (Lambdas, type annotations etc.)

# Version #
 v 0.1
 
 
# How do I get set up? #

* Clone the repository
https://bitbucket.org/maheshsomaraju9/qa-tech-challange

* Switch to branch
feature/MaheshSomaraju_QloyalCodeWebTest

* Download below executables
    - **chromedriver_win32** (http://chromedriver.chromium.org/downloads)
    - **geckodriver-v0.22.0-win64** (https://github.com/mozilla/geckodriver/releases)

* Modify WebAutomation\src\test\resources\thucydides.properties file and configure below properties to correctly refer to executables
    - **webdriver.chrome.driver**
    - **webdriver.gecko.driver**

* Download and install Apache Maven(3.3.9) if not already

* Open Maven Project in IntelliJ IDEA(Preferred) or Eclipse(may need some additional maven setup)

* Configure Proejct SDK (Preferred JDK 1.8)

* Download all Maven dependencies either command line(mvn clean install compile) or in IDE (Maven -> Reimport)

* Build the Project in IDE to ensure the code can be compiled and all necessary dependencies are loaded

* Ensure you have latest Firefox(Preferred) and Chrome installed on your PC



# How to run? #
- Finish the project setup

- There are two Junit Tests available for running Flight and Hotel Search scenarios.
    - **com.quantas.test.travels.booking.features.flights.FlightSearchTests**
    - **com.quantas.test.travels.booking.features.hotels.HotelSearchTests**



- Run the test from IDE by right click -> Run Test

    or

- Run the test from command line using maven as below. Go to command prompt and navigate to **WebAutomation** folder.
```
mvn test -Dtest=com.quantas.test.travels.booking.features.flights.FlightSearchTests
```

- Run below Maven command to clean the reports folder after each run
```
mvn clean
```


- Serenity reports are available at location **WebAutomation\target\site\serenity**

